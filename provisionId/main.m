//
//  main.m
//  provisionId
//
//  Created by Sergey Starukhin on 07.05.15.
//  Copyright (c) 2015 Sergey Starukhin. All rights reserved.
//

#import <Foundation/Foundation.h>

@import Security;

static NSString * const ProvisionIdErrorDomain = @"com.star-s.provisionId.error";

static NSData * DecodeCMS(NSData *cmsData, NSError **error)
{
    NSData *result = nil;
    
    CMSDecoderRef decoder = NULL;
    
    __block OSStatus errCode = CMSDecoderCreate(&decoder);
    
    if (errCode == noErr) {
        //
        [cmsData enumerateByteRangesUsingBlock: ^(const void *bytes, NSRange byteRange, BOOL *stop) {
            errCode = CMSDecoderUpdateMessage(decoder, bytes, byteRange.length);
            *stop = errCode;
        }];
        
        if (errCode == noErr) {
            errCode = CMSDecoderFinalizeMessage(decoder);
        }
        if (errCode == noErr) {
            
            CFDataRef decodedProfileContent = NULL;
            
            errCode = CMSDecoderCopyContent(decoder, &decodedProfileContent);
            
            if (errCode == noErr) {
                result = CFBridgingRelease(decodedProfileContent);
            }
        }
        CFRelease(decoder);
    }
    if (error) {
        *error = result ? nil : [NSError errorWithDomain: ProvisionIdErrorDomain code: errCode userInfo: @{NSLocalizedDescriptionKey: @"error decode cms"}];
    }
    return result;
}

static NSArray * CmdLineArgumentsAsArray(int argc, const char * argv[])
{
    NSMutableArray *result = [NSMutableArray array];
    
    for (int i = 0; i < argc; i++) {
        [result addObject: [NSString stringWithCString: argv[i] encoding: NSUTF8StringEncoding]];
    }
    return result;
}

@interface NSString (SendTo)

- (void)sendToFileHandle:(NSFileHandle *)fileHandle;

- (void)sendToStdOut;

- (void)sendToStdErr;

@end

@implementation NSString (SendTo)

- (void)sendToFileHandle:(NSFileHandle *)fileHandle
{
    [fileHandle writeData: [self  dataUsingEncoding: NSUTF8StringEncoding]];
    [fileHandle writeData: [@"\n" dataUsingEncoding: NSUTF8StringEncoding]];
}

- (void)sendToStdOut
{
    [self sendToFileHandle: [NSFileHandle fileHandleWithStandardOutput]];
}

- (void)sendToStdErr
{
    [self sendToFileHandle: [NSFileHandle fileHandleWithStandardError]];
}

@end

int main(int argc, const char * argv[])
{
    int result = 0;

    @autoreleasepool {
        // insert code here...
        NSError *error = NULL;
        
        NSArray *params = CmdLineArgumentsAsArray(argc, argv);
        
        if (params.count != 2) {
            NSString *errorMsg = [NSString stringWithFormat: @"Usage: %@ <provisioning profile file>", [params firstObject]];
            error = [NSError errorWithDomain: ProvisionIdErrorDomain code: 1 userInfo: @{NSLocalizedDescriptionKey: errorMsg}];
        }
        NSData *data = NULL;
        if (!error) {
            NSString *filePath = [params lastObject];
            filePath = [[filePath stringByDeletingPathExtension] stringByAppendingPathExtension: @"mobileprovision"];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath: filePath isDirectory: NO]) {
                data = [NSData dataWithContentsOfFile: filePath options: kNilOptions error: &error];
            } else {
                NSString *errorMsg = [NSString stringWithFormat: @"File %@ not found", filePath];
                error = [NSError errorWithDomain: ProvisionIdErrorDomain code: 2 userInfo: @{NSLocalizedDescriptionKey: errorMsg}];
            }
        }
        if (!error) {
            data = DecodeCMS(data, &error);
        }
        if (!error) {
            NSPropertyListFormat plistFormat;
            NSDictionary *profile = [NSPropertyListSerialization propertyListWithData: data options: NSPropertyListImmutable format: &plistFormat error: &error];
            
            if (!error) {
                @try {
                    NSUUID *uuid = nil;
                    
                    NSString *identifier = profile[@"UUID"];
                    
                    if (identifier) {
                        uuid = [[NSUUID alloc] initWithUUIDString: identifier];
                    }
                    if (uuid) {
                        [[uuid UUIDString] sendToStdOut];
                    } else {
                        [NSException raise: ProvisionIdErrorDomain format: @"Unknown file format"];
                    }
                }
                @catch (NSException *exception) {
                    error = [NSError errorWithDomain: exception.name code: 3 userInfo: @{NSLocalizedDescriptionKey: exception.reason}];
                }
            }
        }
        if (error) {
            [error.localizedDescription sendToStdErr];
            result = (int)error.code;
        }
    }
    return result;
}
